# Modified DVB Inspector
This modified version support IDR view in the frame graph and add ts analysis log dump support

## Build
You will need to install Maven to build the application and library

		mvn package

## Usage
Open GUI for analysis:

		java -jar DVBinspector-1.3.0.jar

Dump analysis log file (ts_analysis.txt) without GUI:

		java -jar DVBinspector-1.3.0.jar <file-to-analyze.ts>

# Link
Official DVB Inspector can be downloaded at http://www.digitalekabeltelevisie.nl/dvb_inspector/download.shtml
