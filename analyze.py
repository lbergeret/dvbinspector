#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os, sys
import subprocess
import datetime
from optparse import OptionParser
import glob
import re

slice_type = {
    "IDR": 8,
	"I": 7,
	"P": 5,
	"B": 6,
}

# -----------------------------------------------------------------------------
# Utils
# -----------------------------------------------------------------------------
def datetimeToMillisecond(dtime):
  return dtime.hour * 60 * 60 * 1000 + dtime.minute * 60 * 1000 + dtime.second * 1000 + dtime.microsecond/1000

def getParsedData(tsfilename):
	print "Analyzing %s ..." % tsfilename
	p = subprocess.Popen(["java", "-jar", "DVBinspector-1.3.0.jar", "%s" % tsfilename], stderr=open(os.devnull, "w"))
	return_code = p.wait()
	if not return_code:
		data = open("ts_analysis.txt", "r").readlines()
		print "Done."
	else:
		data = None
		print "Failed."
	return data

def processParsedData(data, name, segmentCount):
	currentPTS = 0
	currentSize = 0
	frameCount = 0
	isIDR = False
	result = []
	for l in data:
		l = l.strip()

		# New frame starts
		pos = l.find("pts=")
		if pos != -1:
			currentPTS = l[pos+4:pos+16]
			currentPTS = datetimeToMillisecond(datetime.datetime.strptime(currentPTS, '%H:%M:%S.%f'))

		# Frame size
		pos = l.find("Actual PES length")
		if pos != -1:
			tmpString = l[pos+17:]
			currentSize = int(tmpString[tmpString.find("(")+1:tmpString.find(")")])

		# NAL type
		pos = l.find("nal_unit_type: 0x1")
		if pos != -1:
			isIDR = False

		pos = l.find("nal_unit_type: 0x5")
		if pos != -1:
			isIDR = True

		# Slice info
		pos = l.find("slice_type:")
		if pos != -1:
			frameType = l[-1:]
			if isIDR: 
				frameType = "IDR"
				count = "%s" % frameCount
				print """ffmpeg -i "%s" -vf "select=gte(n\,%d)" -vframes 1 idr_%03d_%03d.png""" % (name, frameCount, segmentCount, frameCount)
			else:
				count = ""
			sizeData = { False: 0, True: currentSize}
			columns = ["%d" % sizeData[frameType==i] for i in slice_type]
			result.append( "%.3f,%s,%d,%s" % (currentPTS/1000.0, ",".join(columns), currentSize, count) )
			frameCount+=1

	return result

# -----------------------------------------------------------------------------
# natural sorting
# http://stackoverflow.com/questions/4623446/how-do-you-sort-files-numerically
# -----------------------------------------------------------------------------
def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    l.sort(key=alphanum_key)

# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == '__main__':
  parser = OptionParser("Usage: analyze.py [options] files")
  parser.add_option("-g", "--graph",
                    action="store_true", dest="graph", default=False,
                    help="render graph")
  parser.add_option("-t", "--time",
                    action="store_true", dest="time", default=False,
                    help="check time")

  (options, args) = parser.parse_args()

  if len(args) != 1:
      parser.error("Wrong number of arguments")

  filesList = glob.glob(args[0])
  sort_nicely(filesList)

  if filesList == []:
  	print "No file found !!!"
  	sys.exit(0)

  result = []
  segmentCount = 1
  output = open("analysis.txt", "w")
  output.close()
  for f in filesList:
  	rawdata = getParsedData(f)
  	result += processParsedData(rawdata, f, segmentCount)
  	segmentCount += 1

  	output = open("analysis.txt", "a")
	output.writelines(rawdata)
	output.close()

  output = open("analysis.csv", "w")
  output.write("pts,"+",".join(slice_type)+",size,count\n")
  for l in result:
  	output.write("%s\n" % l)
  output.close()